---
title: "TerminalFace - A Garmin Watch Face"
date: 2020-03-02T23:14:41.716005+13:00
categories: ["Development"]
metaDescription: ""
metaImageURL: ""
---

__[Source Code: Gitlab](https://gitlab.com/Finnito/terminal-face)__

I got the idea to make a Garmin watch face from [a Reddit post](https://i.imgur.com/E5lNLmA.jpg) for the Samsung Gear that displayed some information in a terminal like way.

After installing Eclipse - a piece of software that very much does not feel at home on the Mac - I got to task. So far there hasn't been much real programming, just laying stuff out with XML and writing a rather monolithic `onUpdate` function in my `TerminalFaceView.mc` controller.

I haven't loaded it onto my __Fēnix 6 Pro__ yet, but in the simulator it looks like this!

<figure>
  <img src="https://gitlab.com/Finnito/terminal-face/-/raw/master/mockup.png" alt="Mockup of TerminalFace on a Garmin Fēnix 6."/>
  <figcaption>(📷: Finn LeSueur) Mockup of TerminalFace on a Garmin Fēnix 6.</figcaption>
</figure>
