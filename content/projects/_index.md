---
title: My Projects
date: 2020-03-02
menu: main
name: "Projects"
weight: 3
metaDescription: "In my spare time I write code - nothing groundbreaking, but you find out about some of my projects here."
metaImageURL: ""
---

# My Projects

In my spare time I write code - nothing groundbreaking, but you can find out about some of my projects here.
