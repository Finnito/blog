---
title: "Edwards-Hawdon"
slug: "edwards-hawdon"
date: 2020-05-30T20:29:18+12:00
categories: ["Hiking"]
type: "post"
draft: true
layout: "multiGPS"
GPXFiles: [
    "Edwards_Hawdon_Day_1_Edwards_Hut.gpx",
    "Edwards_Hawdon_Day_2_Hawdon_Hut.gpx",
    "Edwards_Hawdon_Day_3_Out.gpx"
]
metaDescription: ""
metaImageURL: "posts/edwards-hawdon/"
---

__Hikers:__ Finn, Jen & Tom (May 30th, 2020)

## Day 1: Greyneys Shelter to Edwards Hut

<div id="Edwards_Hawdon_Day_1_Edwards_Hut"></div>

Start the hike by sneaking across State Highway 73 and through the pedestrian tunnel under the train tracks. From here follow the gravel track westward until the road starts to bend away. Here you can cross the Bealey River and follow the trail heading up The Mingha River in the short grass for a short while.

You should take a right turn, crossing The Mingha River just before the Edwards River joins. There is a trail in the bush on the south side of the Edwards Valley - hopefully you can spot the large orange triangle across the river!

The Bealey River can be formiddable in high flows, but we encountered it after a significant dry period, measuring approximately 60 cubic meters per second (cumecs) at the Waimakariri Bridge. You should not have any trouble crossing at this flow. Nor should you with the Edwards River East Branch crossing further up bother you.

From here the track is well-marked, with some riverbed travel indicated by the ever-trusty cairns and a couple of arrows. You should not at any point have to cross the river, preferring to stay on the south-east side at all times.

Shortly before the Edwards River East Branch merges with the main river there is a section of track on the bank to avoid the river which is hard up against the true left (facing downstream) bank of the valley, so keep an eye out!

Immediately after crossing the Edwards River East Branch the track climbs steeply and unforgivingly, save for short sharp desents for most of the way to Edwards Hut. We found this hard and warm work, even in the winter temperatues.

Keep an eye out for great views up and dowm the valley, some large landslides  and a great waterfall!

Edwards Hut is a well fitted out 16-bunk hut with fire, two tables and some triple-bunks! Enjoy it.

## Day 2: Edwards Hut to Hawdon Hut

<div id="Edwards_Hawdon_Day_2_Hawdon_Hut"></div>

### To Taruahuna Pass

The day starts with a very gentle climb, following a generally well-formed path through the tussocks. It is occasionally hard to find the path but just keep heading up the valley and stay on the true left of Edwards River.

If the weather is warm, or the river very low, you can follow the orange markers. This would have you make mang unnecessary crossings which can be avoided!

I forgot how to walk, kicked a rock and went down. Dosed up on painkillers  but feeling pretty stupid, we plodded on.

The trusy cairns will show you the way.

### To Tarn Col

We lost the trail as we came up to the pass, so instead we chose to stick to the east side of the valley heading towards the climb Tarn Col.

This was Jen's favourite part of the day, hopping across the boulders like a bit of a mountain goat. The frost was yeilding to the sun but the rocks were very trecerous if you weren't careful!

The climb to Tarn Col is very steep but there are a few good options to get up there.

- __The left__: Jen and I went this way, it was unrelenting but there wete many tussocks to grab onto and it always feel pretty sturdy.
- __Middle__: Tom took this one and it is probably the most walked route. It goes up the waterfall (not so good in the rain?), lots of tussocks to
hold onto and there are some steps kicked into the hillside.
- __Right__: Dry all the way, some steps kicked into the dirt and seemed pretty good according to some fellow trampers.

### To Walker Pass

The descent here is very steep all the way down to Otehake River East Branch but extremely beautiful. It was especially slow moving for us as there was snow and ice up at Tarn Col and it was very frosty the whole way down. It's worth taking your time. You will have to cross the river a few times
but we avoided wet feet through this section!

At the bottom of the descent you will meet the Otehake River, and will have to cross it a few times during the climb.

### To Hawdon Hut

From the pass the track makes its way through the valley floor, crossing the Twin Fall Stream many times and it is generally very boggy. Wet feet probably unavoidable!

The tarn at Walker Pass is amazing. A great place for a break and snack. The track climbs steeply and briefly before descending steeply into Hawdon Valley. This section was very slow going due to the permafrost on the rocks and dirt, not to mention my severely sprained ankle (again! 🚑).

Thankfully the track flattens out in the last 10 minutes and is extremely beautiful in the dense forest.

## Day 3: Hawdon Hut to Hawdon Shelter

<div id="Edwards_Hawdon_Day_3_Out"></div>



### References

- 

