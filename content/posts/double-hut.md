---
title: Double Hut
date: 2019-03-03T09:08:12+13:00
categories: ["Hiking"]
type: "post"
layout: "gps"
GPXFileURL: "/gpx/2019-01-01-double-hut.gpx"
---

__Hikers:__ Finn, Seonaid, Jay

Double Hut is a very relaxed 10.5 km, 2 hr 15 min walk from the carpark by Lake Heron situated in the Ashburton Lakes District. It is a gentle incline on a well formed track and makes for a nice day trip; extra suitable for families!

We started our walk from the first carpark we found but later realised we could have saved 15-20 minutes walking if we had followed the gravel road a bit further, so write that down! The track generally follows the edge of Lake Heron for a while before heading inland a little towards the hills. Never fear, you don't actually go up them though! Double Hut is situated in a little valley near the valley floor, so there is no significant climbing to be done.

None of us would describe this walk as particularly exciting or interesting, but seeing as we were just getting back into the hiking in the new year it suited us perfectly. We needed to work on our fitness a little bit!

Being a long drive from Christchurch (circa 2 hours), you will spend as much time driving as you will walking, so take that into account if you dislike being in the car!