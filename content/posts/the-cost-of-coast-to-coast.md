---
title: The Cost of Coast to Coast
date: 2019-10-16
categories: ["Sport"]
draft: true
---

Coast to Coast is a 243km multisport race from the West Coast of New Zealand to Christchurch on the East Coast. The event, how I trained for it and what is like is a story for another post. Here, though, is how much it cost me.

| Item                | Category  | Price     |
|:--------------------|:----------|:----------|
| Event Entry         | Entry     | $1,014.90 |
| Van                 | Transport | $3,800.00 |
| Van Registration    | Transport |           |
| Running Vest        | Run       |           |
| Kayak & Spray Skirt |           |           |
| Paddle              |           |           |
| Helmet              |           |           |
| PFD                 |           |           |
| Boat Shoes          |           |           |
| Jacket              |           |           |
| Gloves              |           |           |
| Garmin Watch        |           |           |
| Road Bike           |           |           |
| Helmet              |           |           |
| Bike Shoes          |           |           |
| Speedcross 4s       |           |           |
| Sense Ride 7s       |           |           |
| Socks               |           |           |
| Thermals            |           |           |
| Running Leggings    |           |           |
| Kayak Cradle        |           |           |
| Cross Bars          |           |           |
| Bike Rack           |           |           |
| Headlight           |           |           |
| Beanie              |           |           |
| Running Jacket      |           |           |
| Cycling Gloves      |           |           |
| Grade 2 Certificate |           |           |
| Front Bike Light    |           |           |
| Back Bike Light     |           |           |
| Spare Inner Tube    |           |           |
| Bike Services       |           |           |
| Airbags             |           |           |
| Nose Plug           |           |           |
| Glasses             |           |           |
| Bladder             |           |           |
| Drink Bottles       |           |           |
| Warrant of Fitness  |           |           |
| Registration        |           |           |
| Coaching            |           |           |
| Physio              |           |           |
| Indoor Bike Trainer |           |           |

