---
title: "Brass Monkey Bivvy"
slug: “”
date: 
categories: ["Hiking"]
type: "post"
draft: true
layout: "multiGPS"
GPXFiles: [
    "/posts/brass-monkey-bivvy/Day_1_Brass_Monkey_Bivvy_via_Lewis_Tops.gpx",
    "/posts/brass-monkey-bivvy/Day_2_Out_via_Lucretia_Bivvy.gpx"
]
metaDescription: ""
metaImageURL: "posts/"
---

__Hikers:__ Finn & Jen (March 7th, 2020)

## Day 1: Brass Monkey Bivvy via Lewis Tops

<div id="Day_1_Brass_Monkey_Bivvy_via_Lewis_Tops"></div>

<figure>
  <img src="/posts/brass-monkey-bivvy/IMG_7936.jpg" alt="Jen on the Lewis Tops"/>
  <figcaption>(📷: Finn LeSueur) Jen on the Lewis Tops</figcaption>
</figure>

### References

- [Wilderness Mag: Brass Monkey - Lewis Tops][1]

[1]: https://www.wildernessmag.co.nz/trip/brass-monkey-bivouac-lewis-pass-tops-canterbury-west-coast/ "Brass Monkey - Lewis Tops"

