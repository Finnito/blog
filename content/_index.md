---
menu: main
name: “Home”
weight: 1
metaDescription: "I am a Science and Physics teacher from Christchurch, New Zealand who writes code in his spare time, but would really rather be outside doing almost any type of sport!"
metaImageURL: "posts/the-heaphy/IMG_3479.jpg"
---
